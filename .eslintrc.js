module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    env: {
        browser: true
    },
    plugins: [
        "svelte3",
        "@typescript-eslint",
    ],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
    ],
    rules: {
        "quotes": [1, "double", { allowTemplateLiterals: true, avoidEscape: true }],
    },
    overrides: [
        {
            files: ["*.svelte"],
            processor: "svelte3/svelte3"
        }
    ],
    settings: {
        "svelte3/typescript": true
    }
}
