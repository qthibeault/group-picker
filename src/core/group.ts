import type { Eq } from "fp-ts/es6/Eq"
import type { Ord } from "fp-ts/es6/Ord"

import { string as S, number as N, date as D, eq, ord } from "fp-ts"

export interface Member {
    id: number,
    name: string
}

export const MemberEq: Eq<Member> = eq.struct({
    id: N.Eq,
    name: S.Eq
})

export type Selection = [Date, Member]

export const SelectionOrd: Ord<Selection> = ord.fromCompare((first, second) => D.Ord.compare(first[0], second[0]))

export interface Group {
    name: string,
    members: Member[],
    selections: Selection[]
}
