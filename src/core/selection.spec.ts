import { selectFrom, saveSelections } from "./selection"
import type { Group, Member } from "./group"

const Joe: Member = { id: 1, name: "Joe" }
const Jill: Member = { id: 2, name: "Jill" }
const Jeff: Member = { id: 3, name: "Jeff" }
const Jess: Member = { id: 4, name: "Jess" }
const group: Group = {
    name: "test group",
    members: [Joe, Jill, Jeff, Jess], 
    selections: [
        [new Date("2021-04-28T00:00:00"), Joe],
        [new Date("2021-04-28T00:00:00"), Jill],
        [new Date("2021-04-21T00:00:00"), Jeff],
        [new Date("2021-04-14:00:00"), Jeff],
        [new Date("2021-04-14:00:00"), Joe]
    ]
}

test("n less than number of members", () => {
    const members = selectFrom(group, 2)

    expect(members).toHaveLength(2)
    expect(members).toContainEqual(Jess)
    expect(members).toContainEqual(Jeff)
})

test("n greater than number of members", () => {
    const members = selectFrom(group, 5)

    expect(members).toHaveLength(4)
    expect(members).toContainEqual(Jess)
    expect(members).toContainEqual(Jeff)
    expect(members).toContainEqual(Jill)
    expect(members).toContainEqual(Joe)
})

test("updated selections after selection", () => {
    const members = selectFrom(group, 2)
    const updatedGroup = saveSelections(group, members)
    const expectedLength = group.selections.length + members.length

    expect(updatedGroup.selections).toHaveLength(expectedLength)
})
