import { ord, array, function as f } from "fp-ts"
import { lens } from "monocle-ts"

import { Member, MemberEq, Group, Selection, SelectionOrd } from "./group"


function latestSelection(member: Member, selections: Selection[]): Selection {
    const selectionFilter = (selection: Selection) => MemberEq.equals(member, selection[1])
    const memberSelections = selections.filter(selectionFilter)

    if (memberSelections.length == 0) {
        return [new Date(0, 0, 0), member] // Return epoch start so that it is less than all other selections
    }

    return memberSelections.reduce((acc, el) => ord.max(SelectionOrd)(acc, el))
}

function orderMembers(group: Group): Member[] {
    const latestSelections = group.members.map(member => latestSelection(member, group.selections))
    const orderedSelection = array.sort(SelectionOrd)(latestSelections)

    return orderedSelection.map(selection => selection[1])
}

export function selectFrom(group: Group, n: number): Member[] {
    return array.takeLeft(n)(orderMembers(group))
}

export function saveSelections(group: Group, selected: Member[]): Group {
    const newSelections: Selection[] = selected.map(member => [new Date(), member])
    const updateSelections = f.pipe(
        lens.id<Group>(),
        lens.prop("selections"),
        lens.modify(selections => [...selections, ...newSelections])
    )

    return updateSelections(group)
}
