import type { Option } from "fp-ts/es6/Option"
import type { IO } from "fp-ts/es6/IO"
import type { Group } from "./group"

import { option, io} from "fp-ts"


function parseGroups(json: Option<string>): Group[] {
    const parse = option.map((str: string) => JSON.parse(str) as Group[])
    const defaultToEmpty = option.getOrElse(() => [] as Group[])

    return defaultToEmpty(parse(json))
}

function readStorage(): Option<string> {
    return option.fromNullable(localStorage.getItem("groups"))
}

export const loadGroups = io.map(parseGroups)(readStorage)

export function storeGroups(groups: Group[]): IO<void> {
    return () => localStorage.setItem("groups", JSON.stringify(groups))
}
