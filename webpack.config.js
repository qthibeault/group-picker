const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const path = require("path")
const preprocess = require("svelte-preprocess")


const prod = process.env.NODE_ENV === "production"
const mode = prod ? "production" : "development"

module.exports = {
    mode,
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: path.join(__dirname, "public"),
    },
    resolve: {
        extensions: [".ts", ".js", ".svelte"],
        alias: {
            "@core": path.resolve(__dirname, "src", "core")
        }
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.svelte$/,
                use: {
                    loader: "svelte-loader",
                    options: {
                        compilerOptions: {
                            dev: !prod
                        },
                        emitCss: prod,
                        hotReload: !prod,
                        preprocess: preprocess({ sourceMap: !prod })
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            },
            {
                test: /node_modules\/svelte\/.*\.mjs$/,
                resolve: {
                    fullySpecified: false
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: "[name].css" }),
        new HtmlWebpackPlugin({ template: "./src/index.html", inject: "body" })
    ],
    optimization: {
        usedExports: true
    },
    devtool: prod ? false : "source-map",
    devServer: {
        contentBase: path.join(__dirname, "public"),
        port: 8080,
        hot: true
    }
}
